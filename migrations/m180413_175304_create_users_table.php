<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180413_175304_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(128)->notNull(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string(32),
        ], $tableOptions);

        $this->createIndex('idx-users-id', 'users', 'id');
        $this->createIndex('idx-users-login', 'users', 'username');

        $this->insert('users', [
            'fio' => 'admin',
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin')
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('users');
    }
}
