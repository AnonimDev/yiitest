<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prodject".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $price
 * @property string $start_date
 * @property string $end_date
 */
class Prodject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prodject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'price', 'start_date', 'end_date'], 'required'],
            [['user_id', 'price'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'price' => 'Price',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }
}
