<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prodject */

$this->title = 'Create Prodject';
$this->params['breadcrumbs'][] = ['label' => 'Prodjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prodject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
